<?php
class MagicMethod
{

     function __construct()
     {
          echo "In constructor <br>";
           $this->property="MagicMethod";
     }
    function __destruct()
    {
        echo "Destroying".$this->property." <br>";
    }


}

$obj = new MagicMethod();